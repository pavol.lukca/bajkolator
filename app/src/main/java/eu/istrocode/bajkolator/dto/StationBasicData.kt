package eu.istrocode.bajkolator.dto

import com.google.gson.annotations.SerializedName

data class StationBasicData(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("title") val title: String? = null,
        @SerializedName("latLng") val latLng: LatLng? = null,
        @SerializedName("docks") val docks: Int? = null
)
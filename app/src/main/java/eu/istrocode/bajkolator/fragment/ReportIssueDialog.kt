package eu.istrocode.bajkolator.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.dto.report.Reason
import eu.istrocode.bajkolator.model.MapViewModel
import eu.istrocode.bajkolator.model.ReportIssueViewModel
import eu.istrocode.bajkolator.util.KeyboardUtil

class ReportIssueDialog : DialogFragment() {

    private var mainModel: MapViewModel? = null
    private var model: ReportIssueViewModel? = null

    private var editBikeNumber: EditText? = null
    private var editReport: EditText? = null
    private var chipGroup: ChipGroup? = null
    private var chipBikeNumber: Chip? = null
    private var buttonSend: Button? = null
    private var buttonDismiss: Button? = null

    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_report_issue, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val station = arguments?.getParcelable<Station>(argStation)
        (view.findViewById<TextView>(R.id.text_title)).text = station!!.title
        buttonSend = (view.findViewById(R.id.button_send_report))
        buttonSend!!.setOnClickListener { mainModel!!.sendIssueReport(model!!.buildReport(station)) }
        buttonDismiss = (view.findViewById(R.id.button_dismiss))
        buttonDismiss!!.setOnClickListener { dismiss() }
        editBikeNumber = view.findViewById(R.id.edit_bike_number)
        editBikeNumber!!.addTextChangedListener(textBikeNumberChanged)
        editBikeNumber!!.setOnFocusChangeListener { v, hasFocus ->
            KeyboardUtil.setImeVisibility(v, hasFocus)
        }
        editReport = view.findViewById(R.id.edit_report_details)
        editReport!!.addTextChangedListener(textReportChanged)
        editReport!!.setOnFocusChangeListener { v, hasFocus ->
            KeyboardUtil.setImeVisibility(v, hasFocus)
        }
        chipGroup = view.findViewById(R.id.issueTypeChipGroup)
        chipBikeNumber = view.findViewById(R.id.chipBikeNumber)
        chipBikeNumber!!.setOnCloseIconClickListener {
            model!!.setBikeNumber(null)
            chipGroup!!.clearCheck()
            editReport!!.text = null
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainModel = ViewModelProviders.of(activity!!).get(MapViewModel::class.java)
        model = ViewModelProviders.of(this).get(ReportIssueViewModel::class.java)
        model!!.getCustomReasonInputVisible().observe(this, Observer {
            if (it) {
                editReport!!.visibility = View.VISIBLE
                editReport!!.requestFocus()
            } else {
                editReport!!.visibility = View.GONE
            }
        })
        model!!.getSendEnabled().observe(this, Observer {
            buttonSend!!.isEnabled = it
        })
        model!!.getBikeInputEnabled().observe(this, Observer {
            if (it) {
                editBikeNumber!!.visibility = View.VISIBLE
                editBikeNumber!!.requestFocus()
                chipBikeNumber!!.visibility = View.INVISIBLE
                chipGroup!!.visibility = View.GONE
            } else {
                editBikeNumber!!.visibility = View.INVISIBLE
                chipBikeNumber!!.visibility = View.VISIBLE
                chipGroup!!.visibility = View.VISIBLE
            }
        })
        model!!.getBikeNumber().observe(this, Observer {
            chipBikeNumber!!.text = it
        })
        model!!.getClearBikeNumberEvent().observe(this, Observer {
            editBikeNumber!!.text = null
        })
        addChips(model!!.getReasons())
        model!!.getActiveReason().observe(this, Observer {
            selectChip(it)
        })
    }

    private fun addChips(reasons: List<Reason>) {
        reasons.forEach { reason ->
            run {
                val chip = Chip(chipGroup!!.context)
                chip.setText(reason.getTextResId(), TextView.BufferType.NORMAL)
                chip.isClickable = true
                chip.isCheckable = true
                chip.tag = reason
                chipGroup!!.addView(chip)
            }
        }
        chipGroup!!.setOnCheckedChangeListener { chipGroup, i ->
            run {
                val chip = chipGroup.findViewById<Chip>(i)
                if (chip != null) {
                    model!!.setActiveReason(chip.tag as Reason)
                } else {
                    model!!.setActiveReason(null)
                }
            }
        }
    }

    private fun selectChip(reason: Reason?) {
        for (chipIndex in 0 until chipGroup!!.childCount) {
            val chip = (chipGroup!!.getChildAt(chipIndex) as Chip)
            if (chip.tag == reason) {
                chip.isChecked = true
            }
        }
    }

    private val textBikeNumberChanged = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            val text = s.toString()
            if (text.length == 4) {
                model!!.setBikeNumber(text)
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    }

    private val textReportChanged = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            model!!.setCustomReasonText(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        editBikeNumber!!.removeTextChangedListener(textBikeNumberChanged)
        editReport!!.removeTextChangedListener(textReportChanged)
    }

    companion object {

        private const val argStation: String = "station"

        fun newInstance(station: Station): ReportIssueDialog {
            val args = Bundle()
            args.putParcelable(argStation, station)
            val fragment = ReportIssueDialog()
            fragment.arguments = args
            return fragment
        }
    }
}

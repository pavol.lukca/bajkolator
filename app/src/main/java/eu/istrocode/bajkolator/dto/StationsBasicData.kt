package eu.istrocode.bajkolator.dto

import com.google.gson.annotations.SerializedName

data class StationsBasicData(
        @SerializedName("stations") val stations: List<StationBasicData> = ArrayList()
)
package eu.istrocode.bajkolator.model

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import eu.istrocode.bajkolator.LocatorApplication
import eu.istrocode.bajkolator.dto.LocationHolder
import eu.istrocode.bajkolator.livedata.LocationLiveData
import javax.inject.Inject

class LocationViewModel : ViewModel() {

    private val locationLiveData: LocationLiveData

    @Inject
    lateinit var application: Application

    val data: LiveData<LocationHolder>
        get() = locationLiveData

    init {
        LocatorApplication.INSTANCE.component.inject(this)
        locationLiveData = LocationLiveData(application)
    }
}

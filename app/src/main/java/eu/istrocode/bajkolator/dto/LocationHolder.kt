package eu.istrocode.bajkolator.dto

import android.location.Location

class LocationHolder(val location: Location?, val cached: Boolean = false)
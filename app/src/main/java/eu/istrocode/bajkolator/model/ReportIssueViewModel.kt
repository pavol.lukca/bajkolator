package eu.istrocode.bajkolator.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.dto.report.IssueReport
import eu.istrocode.bajkolator.dto.report.OtherReason
import eu.istrocode.bajkolator.dto.report.Reason
import eu.istrocode.bajkolator.dto.report.StandardReason
import eu.istrocode.bajkolator.util.SingleLiveEvent
import java.util.Date

class ReportIssueViewModel : ViewModel() {

    private var bikeNumber: MutableLiveData<String> = MutableLiveData()
    private val reason: MutableLiveData<Reason> = MutableLiveData()
    private val customReasonInputVisible: LiveData<Boolean>
    private val sendEnabled: MutableLiveData<Boolean> = MutableLiveData()
    private val bikeInputEnabled: MutableLiveData<Boolean> = MutableLiveData()
    private val reasons: MutableList<Reason> = ArrayList()
    private val clearBikeNumberEvent: SingleLiveEvent<Void> = SingleLiveEvent()
    private var customReasonText: String? = null

    init {
        setReasons()
        customReasonInputVisible = Transformations.map(reason) {
            it is OtherReason
        }
        bikeInputEnabled.value = true
    }

    fun getCustomReasonInputVisible(): LiveData<Boolean> {
        return customReasonInputVisible
    }

    fun getBikeNumber(): LiveData<String> {
        return bikeNumber
    }

    fun getReasons(): List<Reason> {
        return reasons
    }

    fun setBikeNumber(text: String?) {
        bikeNumber.value = text
        bikeInputEnabled.value = text.isNullOrEmpty()
        if (text.isNullOrEmpty()) {
            clearBikeNumberEvent.call()
            reason.value = null
        }
        refreshSendButton()
    }

    fun getClearBikeNumberEvent(): LiveData<Void> {
        return clearBikeNumberEvent
    }

    fun setCustomReasonText(text: String?) {
        customReasonText = text
        refreshSendButton()
    }

    fun getSendEnabled(): LiveData<Boolean> {
        return sendEnabled
    }

    fun getBikeInputEnabled(): LiveData<Boolean> {
        return bikeInputEnabled
    }

    private fun setReasons(): List<Reason> {
        reasons.add(StandardReason(R.string.issue_reason_cannot_undock))
        reasons.add(StandardReason(R.string.issue_reason_display_off))
        reasons.add(StandardReason(R.string.issue_reason_mechanical_damage))
        reasons.add(StandardReason(R.string.issue_reason_flat_tire))
        reasons.add(OtherReason())
        return reasons
    }

    fun getActiveReason(): LiveData<Reason?> {
        return reason
    }

    fun setActiveReason(reason: Reason?) {
        this.reason.value = reason
        refreshSendButton()
    }

    private fun refreshSendButton() {
        if (bikeNumber.value != null && reason.value != null) {
            if (reason.value is OtherReason) {
                sendEnabled.value = !customReasonText.isNullOrEmpty()
                return
            } else {
                sendEnabled.value = true
            }
        } else {
            sendEnabled.value = false
        }
    }

    fun buildReport(station: Station): IssueReport {
        return IssueReport(
                station = station,
                bikeNumber = bikeNumber.value!!,
                message = customReasonText,
                reason = this.reason.value!!,
                timestamp = Date()
        )
    }
}

package eu.istrocode.bajkolator.dto

import com.google.gson.annotations.SerializedName

data class LatLng(
        @SerializedName("latitude") val latitude: Double? = null,
        @SerializedName("longitude") val longitude: Double? = null
)
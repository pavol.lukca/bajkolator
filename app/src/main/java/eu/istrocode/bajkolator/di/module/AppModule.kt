package eu.istrocode.bajkolator.di.module

import android.app.Application
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import eu.istrocode.bajkolator.api.StationMapConverterFactory
import eu.istrocode.bajkolator.api.WebService
import eu.istrocode.bajkolator.preferences.Prefs
import eu.istrocode.bajkolator.ssl.OkHttpUtills.enableTls12OnPreLollipop
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class AppModule {

    @Singleton
    @Provides
    fun provideService(): WebService {
        val client: OkHttpClient = enableTls12OnPreLollipop(OkHttpClient.Builder()).build()

        return Retrofit.Builder()
                .baseUrl("https://slovnaftbajk.sk")
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(StationMapConverterFactory.create())
                .client(client)
                .build()
                .create(WebService::class.java)
    }

    @Provides
    @Singleton
    internal fun providePreference(application: Application): Prefs {
        return Prefs.getInstance(application)
    }
}

package eu.istrocode.bajkolator.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import eu.istrocode.bajkolator.LocatorApplication
import eu.istrocode.bajkolator.di.module.AppModule
import eu.istrocode.bajkolator.model.LocationViewModel
import eu.istrocode.bajkolator.model.MapViewModel
import javax.inject.Singleton

@Singleton
@Component(
        modules = [AppModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun application(): Application

    fun inject(locatorApplication: LocatorApplication)
    fun inject(mapViewModel: MapViewModel)
    fun inject(locationViewModel: LocationViewModel)
}

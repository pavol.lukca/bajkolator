package eu.istrocode.bajkolator.preferences

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager

import eu.istrocode.bajkolator.R

class Prefs {

    var userPin: String?
        get() = read(
                application!!.getString(R.string.user_pin_key),
                null
        )
        set(value) = write(
                application!!.getString(R.string.user_pin_key),
                value
        )

    var locationSettingsRefused: Boolean
        get() = read(
                application!!.getString(R.string.location_settings_refused_key),
                application!!.resources.getBoolean(R.bool.default_location_settings_refused)
        )
        set(value) = write(
                application!!.getString(R.string.location_settings_refused_key),
                value
        )

    companion object {

        private var prefs: Prefs? = null
        private var sharedPreferences: SharedPreferences? = null

        private var application: Application? = null

        @Synchronized
        fun getInstance(application: Application): Prefs {
            if (prefs == null) {
                Prefs.application = application
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
                prefs = Prefs()
            }
            return prefs as Prefs
        }

        fun read(key: String, defValue: String?): String? {
            return sharedPreferences!!.getString(key, defValue)
        }

        private fun write(key: String, value: String?) {
            val prefsEditor = sharedPreferences!!.edit()
            prefsEditor.putString(key, value)
                    .apply()
        }

        fun read(key: String, defValue: Boolean): Boolean {
            return sharedPreferences!!.getBoolean(key, defValue)
        }

        fun write(key: String, value: Boolean) {
            val prefsEditor = sharedPreferences!!.edit()
            prefsEditor.putBoolean(key, value)
                    .apply()
        }
    }
}

package eu.istrocode.bajkolator.maps

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.util.StationUtil.Companion.getMarekerRes

class StationRenderer(private val context: Context) {

    private val iconGenerator = com.google.maps.android.ui.IconGenerator(context)

    @SuppressLint("InflateParams")
    fun getClusterItemIcon(station: Station): BitmapDescriptor {
        val layout = LayoutInflater.from(context).inflate(R.layout.layout_icon, null)

        if (station.bikes != null) {
            layout.findViewById<TextView>(R.id.text).text = station.bikes.toString()
        } else {
            layout.findViewById<TextView>(R.id.text).height = context.resources.getDimension(R.dimen.unavailable_station_icon_size).toInt()
        }
        iconGenerator.setContentView(layout)
        iconGenerator.setBackground(null)

        val backgroundRes: Int = getMarekerRes(station)
        val drawable = ContextCompat.getDrawable(context, backgroundRes)
        if (Build.VERSION.SDK_INT >= 16) {
            layout.background = drawable
        } else {
            //noinspection deprecation
            layout.setBackgroundDrawable(drawable)
        }
        return BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon())
    }
}
package eu.istrocode.bajkolator.fragment

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.model.MapViewModel

class ShowPinDialog : DialogFragment() {

    private lateinit var mapViewModel: MapViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        mapViewModel = ViewModelProviders.of(activity!!).get(MapViewModel::class.java)
        return AlertDialog.Builder(context!!)
                .setTitle(splitPeriodic(mapViewModel.userPin!!))
                .setNegativeButton(R.string.dialog_remove_confirm) { dialogInterface, _ ->
                    RemovePinDialog.newInstance().show(activity!!.supportFragmentManager, REMOVE_PIN_DIALOG_TAG)
                    dialogInterface.cancel()
                }
                .setPositiveButton(R.string.dialog_dismiss) { dialogInterface, _ ->
                    dialogInterface.cancel()
                }
                .create()
    }

    companion object {

        fun splitPeriodic(text: String): String {
            return text.replace("....".toRegex(), "$0 ")
        }

        fun newInstance(): DialogFragment {
            return ShowPinDialog()
        }

        private const val REMOVE_PIN_DIALOG_TAG = "REMOVE_PIN_DIALOG"
    }
}
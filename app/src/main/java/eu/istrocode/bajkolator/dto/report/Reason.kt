package eu.istrocode.bajkolator.dto.report

interface Reason {
    fun getTextResId(): Int
}
package eu.istrocode.bajkolator.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.model.MapViewModel
import eu.istrocode.bajkolator.util.DateUtils
import eu.istrocode.bajkolator.util.StationUtil

class StationDetailDialog : DialogFragment() {

    var model: MapViewModel? = null

    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_station_detail, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val station = arguments?.getParcelable<Station>(StationDetailDialog.argStation)

        val textStationStatus = view.findViewById<TextView>(R.id.text_station_status)
        (view.findViewById<TextView>(R.id.text_title)).text = station!!.title
        val textTime = (view.findViewById<TextView>(R.id.text_time))
        val imageTime = (view.findViewById<ImageView>(R.id.image_time))
        textStationStatus.text = getString(R.string.station_capacity_status,
                station.bikes ?: "?", station.docks)
        textStationStatus.setTextColor(ContextCompat.getColor(context!!, StationUtil.getColorRes(station)))
        if (station.refresh != null) {
            textTime.visibility = View.VISIBLE
            textTime.setText(DateUtils.formatTimeSpannable(station.refresh), TextView.BufferType.SPANNABLE)
            imageTime.visibility = View.VISIBLE
        } else {
            textTime.visibility = View.GONE
            imageTime.visibility = View.GONE
        }
        val button: Button = view.findViewById(R.id.button_create_report)
        button.setOnClickListener {
            model!!.showReportIssueDialog(station)
            dismiss()
        }
        val buttonNavigate: Button = view.findViewById(R.id.button_navigate)
        buttonNavigate.setOnClickListener {
            model!!.navigateToStation(station)
            dismiss()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model = ViewModelProviders.of(activity!!).get(MapViewModel::class.java)
    }

    companion object {

        private const val argStation: String = "station"

        fun newInstance(station: Station): StationDetailDialog {
            val args = Bundle()
            args.putParcelable(argStation, station)
            val fragment = StationDetailDialog()
            fragment.arguments = args
            return fragment
        }
    }
}